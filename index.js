let http = require('http');

let course = [
	{
		"name": 'Javascript 101',
		"description": 'Introduction to Javascript',
	},
	{

		"name": 'HTML and CSS 101',
		"description": 'Introduction to HTML and CSS',
	}
];

let port = 8080

let server = http.createServer(function(request, response) {
	if(request.url == '/' && request.method == 'GET') {
		response.writeHead(200, {'Content-Type': 'application/json'})
		response.end('Welcome to booking system.')
	} if(request.url == '/profile' && request.method == 'GET') {
		response.writeHead(200, {'Content-Type': 'application/json'})
		response.end('Welcome to your profile.')
	} if(request.url == '/courses' && request.method == 'GET') {
		response.writeHead(200, {'Content-Type': 'application/json'})
		response.end("Here's our courses available.")
	} if(request.url == '/addCourse' && request.method == 'POST') {
		let request_body = ''

		request.on('data', function(data) {
			request_body += data
		})
		request.on('end', function() {
			console.log(typeof request_body)
			request_body = JSON.parse(request_body)

			let new_course = {
				"name": request_body.name,
				"description": request_body.description
			}

			course.push(new_course)
			console.log(course)

			response.writeHead(200, {'Content-Type': 'application/json'})
			response.write(JSON.stringify(new_course))
			response.end()
		})

	}
});

server.listen(port);

console.log(`Server is running on localhost: ${port}`);